#include <stdio.h>

/*suma y multiplicacion de dos numeros
  Autor: Alejandro Garay Ramirez
  16/Octubre/2018*/

int main(int argc, char const *argv[]) {

    int a,b,c,d = 0;

    printf("Dame un numero: ");
    scanf("%i",&a);
    printf("Dame otro numero: ");
    scanf("%i",&b);

    c = a+b;
    d = a*b;

    printf("\nLa suma de ambos numeros es: %i\n",c);
    printf("La multiplicacion de ambos numeros es: %i\n",d);

  return 0;
}
