#include <stdio.h>

/*Operacion algebraica
  Autor: Alejandro Garay Ramirez
  17/Octubre/2018*/

int main(int argc, char const *argv[]) {

    int a = 10;
    int b = 2;
    int c = 4;

    int d = (a*b+c)*(c-b);

    printf("El Resultado es: %i\n",d);

  return 0;
}
