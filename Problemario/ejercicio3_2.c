#include <stdio.h>

/*suma de 3 numeros
  Autor: Alejandro Garay Ramirez
  16/Octubre/2018*/

  int main(int argc, char const *argv[]) {

      int Numero1 = 0;
      float Numero2,Numero3,suma = 0.0f;

      printf("Dame un numero entero: ");
      scanf("%i",&Numero1);
      printf("Dame un numero con decimal: ");
      scanf("%f",&Numero2);
      printf("Dame otro numero con decimal: ");
      scanf("%f",&Numero3);

      suma = Numero1+Numero2+Numero3;

      printf("\nLa suma de los 3 numeros es: %.3f\n",suma);

    return 0;
  }
