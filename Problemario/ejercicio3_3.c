#include <stdio.h>

/*Suma de 2 numeros enteros
  Autor: Alejandro Garay Ramirez
  16/Octubre/2018*/

  int main(int argc, char const *argv[]) {

      int Numero1, Numero2, Suma = 0;

      printf("Dame un numero: ");
      scanf("%i",&Numero1);
      printf("Dame otro numero: ");
      scanf("%i",&Numero2);

      Suma = Numero1+Numero2;

      printf("La suma de %i mas %i es = %i\n",Numero1,Numero2,Suma);

    return 0;
  }
