#include <stdio.h>

/*Operaciones con 2 numeros
  Autor: Alejandro Garay Ramirez
  16/Octubre/2018*/

  int main(int argc, char const *argv[]) {

      int Numero1, Numero2, Suma, Resta, Mult, Div, Mod = 0;

      printf("Dame un numero: ");
      scanf("%i",&Numero1);
      printf("Dame otro numero: ");
      scanf("%i",&Numero2);

      Suma = Numero1+Numero2;
      Resta = Numero1-Numero2;
      Mult = Numero1*Numero2;
      Div = Numero1/Numero1;
      Mod = Numero1%Numero2;

      printf("La suma de ambos numeros es: %i\n",Suma);
      printf("La resta de ambos numeros es: %i\n",Resta);
      printf("La multiplicacion de ambos numeros es: %i\n",Mult);
      printf("La divicion de ambos numeros es: %i\n",Div);
      printf("El modulo de ambos numeros es: %i\n",Mod);


    return 0;
  }
