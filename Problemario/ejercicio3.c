#include <stdio.h>

/*Imprime dos variables de tipo entero
  Autor: Alejandro Garay Ramirez
  16/Octubre/2018*/

  int main(int argc, char const *argv[]) {

      int Numero1 = 95;
      int Numero2 = 80;

      printf("Imprime mis dos variables\n");
      printf("Mi Numero 1 es: %i y mi Numero 2 es: %i\n",Numero1,Numero2);

    return 0;
  }
