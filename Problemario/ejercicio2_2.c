#include <stdio.h>

/*Imprime el area de un cuadrado
  Autor: Alejandro Garay Ramirez
  16/Octubre/2018*/

int main(int argc, char const *argv[]) {

    int lado, area = 0;

    printf("Dame un lado del triangulo: ");
    scanf("%i",&lado);

    area = lado*lado;
    printf("El area del cuadrado es: %i\n",area);

  return 0;
}
