struct autos {

        char marca[20];
        char nombre[20];
        char traccion[25];
        char placa[10];
        char transmision[15];
};

struct computadora {

        char marca[15];
        char prosesador[15];
        int nucleos;
        int resolucion;
        int cantidaddepuertos;;
};

struct alumno {

        char nombre[20];
        int edad;
        char grupo[4];
        char salud[10];
        char discapacidades[15];
};

struct equipoDeFutbol {

        char nombre[15];
        int jugadores;
        char director[25];
        int trofeos;
        char pais[15];
};

struct mascota {

        char nombre[10];
        int edad;
        char sexo;
        char especie[10];
        float peso;
};

struct videoJuego {

        char nombre[20];
        int jugadores;
        char tematica[15];
        char historia[40];
        char personajeprincipal[15];
        int año;
};

struct superHeroe {

        char nombre[20];
        char enemigos[40];
        char aliados[40];
        char apariciones[40];
        char juegosycomics[15];
};

struct planetas {

        char nombre[20];
        int posicion;
        int tamaño;
        int edad;;
        char secomponede[40];
};

struct personajeDeSujuegoFavorito {

        char nombre[15];
        char juego[15];
        int  estatura;
        char primeraaparicion[20];
        char caracteristicaprincipal;
};

struct comic {

        int numero;
        char nombre[20];
        int año;
        char personajeprincipal[15];
        char autor[20];
};
