#include<stdio.h>
#define LAD 6

/*AlejandroGarayRamirez
  Dibujar triangulo
  2/Nov/2018*/

int main(int argc, char const *argv[]) {

  int altura, mov = 0;

  for (altura = LAD; altura > 0; altura--) {
    for (mov = altura; mov > 0; mov--)
      printf("+");
      for (mov = altura; mov <= LAD; mov++)
        printf(" ");
        printf("\n");
  }

  return 0;
}
