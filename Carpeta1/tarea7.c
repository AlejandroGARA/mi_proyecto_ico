#include <stdio.h>

/* AlejandroGarayRamirez
   suma y resta de dos numeros
   8/Oct/2018*/


int main(int argc, char const *argv[]) {

  int a,b,c,d = 0;

  printf("Ingrese un numero: ");
  scanf("%i",&a);
  printf("Ingrese otro numero: ");
  scanf("%i",&b);

  c = a+b;
  d = a-b;

  printf("\nLa suma de ambos numeros es: %i\n",c);
  printf("\nLa resta de ambos numeros es: %i\n",d);


  return 0;
}
